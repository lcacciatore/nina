﻿<!--
    Copyright © 2016 - 2020 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.-->
<ResourceDictionary
    x:Class="NINA.Sequencer.SequenceItem.FlatDevice.Datatemplates"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:filter="clr-namespace:NINA.Model.MyFilterWheel"
    xmlns:local="clr-namespace:NINA.Sequencer.SequenceItem.FlatDevice"
    xmlns:mini="clr-namespace:NINA.View.Sequencer.MiniSequencer"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:util="clr-namespace:NINA.Utility"
    xmlns:view="clr-namespace:NINA.View.Sequencer">

    <DataTemplate x:Key="NINA.Sequencer.SequenceItem.FlatDevice.SetBrightness_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{Binding Brightness}" />
                    <TextBlock VerticalAlignment="Center" Text="%" />
                </StackPanel>
            </mini:MiniSequenceItem.SequenceItemContent>
        </mini:MiniSequenceItem>
    </DataTemplate>
    <DataTemplate DataType="{x:Type local:SetBrightness}">
        <view:SequenceBlockView DataContext="{Binding}">
            <view:SequenceBlockView.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblFlatDeviceBrightness}" />
                    <ninactrl:StepperControl
                        Margin="5,0,0,0"
                        MaxValue="100"
                        MinValue="0"
                        StepSize="1"
                        Value="{Binding Brightness, Mode=TwoWay}" />
                    <TextBlock VerticalAlignment="Center" Text="%" />
                </StackPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>
    <DataTemplate DataType="{x:Type local:ToggleLight}">
        <view:SequenceBlockView DataContext="{Binding}">
            <view:SequenceBlockView.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <CheckBox VerticalAlignment="Center" IsChecked="{Binding On}" />
                </StackPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate x:Key="NINA.Sequencer.SequenceItem.FlatDevice.TrainedFlatExposure_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemProgressContent>
                <WrapPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblProgress}" />
                    <TextBlock
                        Margin="5,0,0,0"
                        VerticalAlignment="Center"
                        Text="{Binding Items[4].Conditions[0].CompletedIterations}" />
                    <TextBlock VerticalAlignment="Center" Text="/" />
                    <TextBlock VerticalAlignment="Center" Text="{Binding Items[4].Conditions[0].Iterations}" />
                </WrapPanel>
            </mini:MiniSequenceItem.SequenceItemProgressContent>
        </mini:MiniSequenceItem>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:TrainedFlatExposure}">
        <view:SequenceBlockView DataContext="{Binding}">
            <view:SequenceBlockView.SequenceItemContent>
                <WrapPanel DataContext="{Binding Items[4]}" Orientation="Horizontal">
                    <TextBlock
                        Margin="5,0,0,0"
                        VerticalAlignment="Center"
                        Text="{ns:Loc LblAmount}" />
                    <TextBox
                        MinWidth="40"
                        Margin="5,0,0,0"
                        VerticalAlignment="Center"
                        Text="{Binding Conditions[0].Iterations}"
                        TextAlignment="Right" />
                    <TextBlock
                        Margin="10,0,10,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblFilter}" />

                    <ComboBox
                        Margin="5,0,0,0"
                        DisplayMemberPath="Name"
                        SelectedItem="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type view:SequenceBlockView}}, Path=DataContext.Items[2].Filter, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource FilterWheelFilterConverter}}"
                        SelectedValuePath="Name">
                        <ComboBox.Resources>
                            <CollectionViewSource x:Key="Filters" Source="{Binding Source={StaticResource ProfileService}, Path=ActiveProfile.FilterWheelSettings.FilterWheelFilters}" />
                        </ComboBox.Resources>
                        <ComboBox.ItemsSource>
                            <CompositeCollection>
                                <x:Static Member="filter:NullFilter.Instance" />
                                <CollectionContainer Collection="{Binding Source={StaticResource Filters}}" />
                            </CompositeCollection>
                        </ComboBox.ItemsSource>
                    </ComboBox>

                    <TextBlock
                        Margin="10,0,10,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <StackPanel DataContext="{Binding Items[0]}" Orientation="Horizontal">
                        <StackPanel.Resources>
                            <util:BindingProxy x:Key="CameraInfo" Data="{Binding CameraInfo}" />
                        </StackPanel.Resources>

                        <TextBlock
                            Margin="10,0,10,0"
                            HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Text="|" />

                        <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblBinning}" />
                        <ComboBox
                            Margin="5,0,0,0"
                            DisplayMemberPath="Name"
                            ItemsSource="{Binding Source={StaticResource CameraInfo}, Path=Data.BinningModes, Converter={StaticResource DefaultBinningModesConverter}}"
                            SelectedItem="{Binding Binning, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                            SelectedValuePath="Name" />

                        <!--  List of Gain  -->
                        <WrapPanel Orientation="Horizontal">
                            <WrapPanel.Visibility>
                                <PriorityBinding>
                                    <Binding
                                        Converter="{StaticResource CollectionContainsItemsToVisibilityConverter}"
                                        Path="Data.Gains"
                                        Source="{StaticResource CameraInfo}" />
                                    <Binding
                                        Converter="{StaticResource BooleanToVisibilityCollapsedConverter}"
                                        Path="Data.Connected"
                                        Source="{StaticResource CameraInfo}" />
                                </PriorityBinding>
                            </WrapPanel.Visibility>
                            <TextBlock
                                Margin="10,0,10,0"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="|" />
                            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblGain}" />
                            <ComboBox
                                Margin="5,0,0,0"
                                DisplayMemberPath="Text"
                                IsSynchronizedWithCurrentItem="True"
                                SelectedValuePath="Text">
                                <ComboBox.ItemsSource>
                                    <CompositeCollection>
                                        <TextBlock Text="{Binding Source={StaticResource CameraInfo}, Path=Data.DefaultGain, UpdateSourceTrigger=PropertyChanged, StringFormat=({0})}" />
                                        <CollectionContainer Collection="{Binding Source={StaticResource CameraInfo}, Path=Data.Gains, Converter={StaticResource IntListToTextBlockListConverter}}" />
                                    </CompositeCollection>
                                </ComboBox.ItemsSource>
                                <ComboBox.SelectedValue>
                                    <MultiBinding
                                        Converter="{StaticResource MinusOneToBaseValueConverter}"
                                        Mode="TwoWay"
                                        UpdateSourceTrigger="PropertyChanged">
                                        <Binding
                                            Mode="TwoWay"
                                            Path="Gain"
                                            UpdateSourceTrigger="PropertyChanged" />
                                        <Binding
                                            Mode="OneWay"
                                            Path="Data.DefaultGain"
                                            Source="{StaticResource CameraInfo}"
                                            UpdateSourceTrigger="PropertyChanged" />
                                    </MultiBinding>
                                </ComboBox.SelectedValue>
                            </ComboBox>
                        </WrapPanel>

                        <!--  Free Gain  -->
                        <WrapPanel Orientation="Horizontal">
                            <WrapPanel.Visibility>
                                <PriorityBinding FallbackValue="Visible">
                                    <Binding
                                        Converter="{StaticResource InverseCollectionContainsItemsToVisibilityConverter}"
                                        Path="Data.Gains"
                                        Source="{StaticResource CameraInfo}" />
                                </PriorityBinding>
                            </WrapPanel.Visibility>
                            <TextBlock
                                Margin="10,0,10,0"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="|" />
                            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblGain}" />
                            <TextBox
                                MinWidth="40"
                                Margin="5,0,0,0"
                                VerticalAlignment="Center"
                                TextAlignment="Right">
                                <TextBox.Text>
                                    <MultiBinding Converter="{StaticResource MinusOneToBaseValueConverter}" UpdateSourceTrigger="LostFocus">
                                        <Binding
                                            Mode="TwoWay"
                                            Path="Gain"
                                            UpdateSourceTrigger="PropertyChanged">
                                            <Binding.ValidationRules>
                                                <util:ShortRangeRule>
                                                    <util:ShortRangeRule.ValidRange>
                                                        <util:ShortRangeChecker Maximum="32767" Minimum="-1" />
                                                    </util:ShortRangeRule.ValidRange>
                                                </util:ShortRangeRule>
                                            </Binding.ValidationRules>
                                        </Binding>
                                        <Binding
                                            Mode="OneWay"
                                            Path="Data.DefaultGain"
                                            Source="{StaticResource CameraInfo}"
                                            UpdateSourceTrigger="PropertyChanged" />
                                    </MultiBinding>
                                </TextBox.Text>
                            </TextBox>
                        </WrapPanel>

                        <!--  Offset  -->
                        <!--<StackPanel Orientation="Horizontal">
                        <StackPanel.Visibility>
                            <PriorityBinding FallbackValue="Visible">
                                <Binding
                                    Converter="{StaticResource BooleanToVisibilityCollapsedConverter}"
                                    Path="Data.CanSetOffset"
                                    Source="{StaticResource CameraInfo}" />
                            </PriorityBinding>
                        </StackPanel.Visibility>
                        <TextBlock
                            Margin="10,0,10,0"
                            HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Text="|" />
                        <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblOffset}" />
                        <TextBox
                            MinWidth="40"
                            Margin="5,0,0,0"
                            VerticalAlignment="Center"
                            TextAlignment="Right">
                            <TextBox.Text>
                                <MultiBinding Converter="{StaticResource MinusOneToBaseValueConverter}" UpdateSourceTrigger="LostFocus">
                                    <Binding
                                        Mode="TwoWay"
                                        Path="Offset"
                                        UpdateSourceTrigger="PropertyChanged">
                                        <Binding.ValidationRules>
                                            <util:ShortRangeRule>
                                                <util:ShortRangeRule.ValidRange>
                                                    <util:ShortRangeChecker Maximum="{Binding Source={StaticResource CameraInfo}, Path=Data.OffsetMax}" Minimum="{Binding Source={StaticResource CameraInfo}, Path=Data.OffsetMin}" />
                                                </util:ShortRangeRule.ValidRange>
                                            </util:ShortRangeRule>
                                        </Binding.ValidationRules>
                                    </Binding>
                                    <Binding
                                        Mode="OneWay"
                                        Path="Data.DefaultOffset"
                                        Source="{StaticResource CameraInfo}"
                                        UpdateSourceTrigger="PropertyChanged" />
                                </MultiBinding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>-->
                    </StackPanel>
                </WrapPanel>
            </view:SequenceBlockView.SequenceItemContent>
            <view:SequenceBlockView.SequenceItemProgressContent>
                <WrapPanel DataContext="{Binding Items[4]}" Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblProgress}" />
                    <TextBlock
                        Margin="5,0,0,0"
                        VerticalAlignment="Center"
                        Text="{Binding Conditions[0].CompletedIterations}" />
                    <TextBlock VerticalAlignment="Center" Text="/" />
                    <TextBlock VerticalAlignment="Center" Text="{Binding Conditions[0].Iterations}" />
                </WrapPanel>
            </view:SequenceBlockView.SequenceItemProgressContent>
        </view:SequenceBlockView>
    </DataTemplate>
</ResourceDictionary>